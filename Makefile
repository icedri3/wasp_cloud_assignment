
all: instances.png

sparklog.log: commands.sh linalg-measure.py
	./commands.sh

cleanlog.csv: sparklog.log
	cat sparklog-*.log | grep helloworld | grep INFO | perl -pe 's/\x1b\[[0-9;]*[mG]//g' | cut -d'-' -f2 | grep -v tmp > cleanlog.csv

instances.png: cleanlog.csv plot.R
	Rscript plot.R

report:
	make -C report