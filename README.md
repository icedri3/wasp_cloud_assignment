# WASP Software and Cloud Engineering assignment
[Link to
assignment](https://docs.google.com/document/d/1D6xGA-gf5tV6njvIIySMIDXnS7Ay0YAI2eoepXWoX8c/edit)

> As the foremost research team in data science, a guy called Ahmed came to your team and asked you to evaluate for him if he should use Spark on the Google Cloud for improving the performance of his matrix computations instead of just running his computations on his local machine. He said you can use any programming language of your choice to evaluate for him a Spark library called LINALG [4], [5]. As data scientists, you asked Ahmed for test data. He pointed you to the following dataset of Matrices that you can use for your testing [6] where you will find multiple matrices in different formats from many applications, including, control and optimization, networking, and privacy data. To evaluate the library, you need to choose two of the available methods in the library, and one of the matrices available in the datasets. You will then evaluate the speedup when using Spark with a low number of nodes, versus a large number of nodes for each of the methods. Choose the matrix wisely. A very small matrix will show you nothing. A very large one will take forever to operate on (and consume all $300). You will write a report where you will explain your choice of the methods, and the dataset. You will describe your setup, and your conclusion. As scientists, you should be able to describe why you have reached that conclusion, e.g., by showing performance speedup graphs or slowdowns. Please comment on the behaviour you see, if it is linear in the amount of resources, sublinear, or something else, or if the CPU was the bottleneck, or the Memory, etc? Please follow the data science process, and comment on how you have followed it.

![results](instances.png)
 
## References

* [4] https://spark.apache.org/docs/1.5.1/api/java/org/apache/spark/mllib/linalg/package-frame.html 
* [5] https://shivaram.org/publications/matrix-spark-kdd.pdf 
* [6]  https://sparse.tamu.edu/ 

## Links

* https://github.com/apache/spark/tree/master/examples/src/main/python/mllib
* https://github.com/apache/spark/blob/master/examples/src/main/python/mllib/pca_rowmatrix_example.py
* https://towardsdatascience.com/step-by-step-tutorial-pyspark-sentiment-analysis-on-google-dataproc-fef9bef46468


## Notes

* What is the "data science process"?


## Instructions

* Create a project, attach and authenticate. Edit parameters in `commands.sh` to your liking.
* Add Dataproc API to your project. You can do this in the web interface, or by answering yes when asked after running script.
* Run `./commands.sh`.

## TODO

* How to measure time?
* Choose matrix.
* What to do with the matrix?
* How to store matrix in Google storage?
* How to access data in Google storage?

![meme](https://pbs.twimg.com/media/Dq1WGhTX4AE4L3x.jpg) 
